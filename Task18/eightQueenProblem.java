import java.util.ArrayList;
import java.util.Random;

public class eightQueenProblem {
	public static void main(String[] args){
		if (args.length != 1){
			printHelp();
		} else {

			String initialQueen = args[0];

			if (!verifyAlgNotation(initialQueen)){
				printHelp();
			} else {

				char[][] board = generateBoard();

				int[] queen = getCoordinates(initialQueen);


				board = addQueen(queen, board);

				//printBoard(board);

				board = trySolve(board, initialQueen);

				//printBoard(board);
			}



			
		}

	}
	static char[][] generateBoard(){
		char[][] board = new char[8][8];

				for(int i = 0; i < board.length; i++){
					for(int j = 0; j < board[0].length; j++){
						board[i][j] = '#';
					}
				}
		return board;
	}

	static char[][] trySolve(char[][] initialboard, String initialQueen){

		ArrayList<int[]> queens = new ArrayList<int[]>();

		char[][] gameboard = generateBoard();
		int[] pos = getCoordinates(initialQueen); 
		gameboard = findValidArea(gameboard, pos);

		printBoard(gameboard);
		int next=0;
		
		while(true){
			
			int[] queen = validPlacement(gameboard,next);
			



			if (validPlacement(gameboard,next)[0] == -1){
				//System.out.println("Valid placement:" + validPlacement(gameboard,next)[0]);
				System.out.println("Please backtrack");
				gameboard = removeQueen(gameboard,queens);
				if (queens.size() > 1){
					queens.remove(queens.size()-1);
				} else {
					System.out.println("no solution");
					break;
				}	
				//System.out.println(queens);
				next++;
			} else {
				System.out.println("Continuing");
				next=new Random().nextInt(10);
				queens.add(queen);
				gameboard = addQueen(queen, gameboard);
				gameboard = findValidArea(gameboard, queen);
				printBoard(gameboard);	

				if (queens.size() == 8){
					System.out.println("solution found");
					printBoard(gameboard);
					break;
				}

			}
		}
		return gameboard;
	}



	static char[][] removeQueen(char[][] gameboard, ArrayList<int[]> queens){
		gameboard=generateBoard();
		for(int i =0; i< queens.size()-1; i++){
			gameboard=addQueen(queens.get(i),gameboard);
			gameboard=findValidArea(gameboard,queens.get(i));
		}
		//printBoard(gameboard);
		return gameboard;
	}

	static int[] validPlacement(char[][] gameboard, int next){
		int h=0;
		for (int i = 0; i < gameboard.length; i++){
			for (int j = 0; j < gameboard[0].length; j++){
				if (gameboard[i][j] == '#'){
					if(h==next){
						//System.out.println("Next:" + h + " " + next);
						return new int[] {i,j};
					}h++;
				}
			}
				
		}

		return new int[] {-1,-1};
	}

	static char[][] findValidArea(char[][] gameboard, int[] pos){



		
		//System.out.println("pos queen: " + pos[0] + pos[1]);

		for(int i=gameboard.length-1; i>=0;i--){
			for(int j=0; j<gameboard[0].length;j++){

				if (i == pos[0]){
					gameboard[i][j] = 'x';
				}
				if (j == pos[1]){
					gameboard[i][j] = 'x';
				}
				if (j == pos[1] && i == pos[0]){
					gameboard[i][j] = 'Q';
					for (int k = 1; k < 8; k++){
						if (pos[0]-k >=0 && pos[1]-k >=0){
							gameboard[pos[0]-k][pos[1]-k] = 'x';
						}
						if (pos[1]+k <=7 && pos[0]+k <=7){
							gameboard[pos[0]+k][pos[1]+k] = 'x';
						}
						if (pos[0]-k>=0 && pos[1]+k<=7){
							gameboard[pos[0]-k][pos[1]+k]='x';
						}
						if (pos[0]+k <= 7 && pos[1]-k >= 0){
							gameboard[pos[0]+k][pos[1]-k]='x';
						}
						
					}

				}
			}
		}
		return gameboard;
	}

	static int[] getCoordinates(String queen){
		int i = Character.getNumericValue(queen.charAt(1))-1;

		int j = 0; // mellom 0-9
		int algLetter = (int)queen.charAt(0);

		if (algLetter >= 65 && algLetter <= 72){
			j = algLetter - 65;
		}
		else if (algLetter >= 97 && algLetter <= 104){
			j = algLetter - 97;
		}
		int[] ret = new int[2];
		ret[0]=i;
		ret[1]=j;
		System.out.printf("i= %s, j=%s %n",i,j);
		return ret;
	}
/*
	static char[][] addQueen(String queen, char[][] board){
		int[] cords = getCoordinates(queen);

		board[cords[0]][cords[1]] = 'Q';

		return board;
	}*/

	static char[][] addQueen(int[] cords, char[][] board){
		board[cords[0]][cords[1]] = 'Q';

		return board;
	}


	static void printBoard(char[][] board){
		for (int i = board.length-1; i >= 0; i--){
			System.out.print((i+1) + " ");
			System.out.println(board[i]);
		}
		System.out.println("  ABCDEFGH");
	}

	static void printHelp(){
		System.out.println("Usage: write the coordinate you want to check in algeraic notation");
	}

	static boolean verifyAlgNotation(String s){
		if (s.length() != 2 ){
			return false;
		} else{
			int algLetter = (int)s.charAt(0); //65 - 72 (A-H) 97-104 (a-h)
			int algNumber = Character.getNumericValue(s.charAt(1));

			if ((algLetter >= 65 && algLetter <= 72 ) || (algLetter >= 97 && algLetter <= 104)){
				if (algNumber >= 1 && algNumber <= 8){
					return true;
				}
			}
			return false;
		}
	}


}