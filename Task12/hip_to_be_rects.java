class hip_to_be_rects{
    public static void main(String[] args){
        try{
            char[][]rect=new char[Integer.parseInt(args[0])][Integer.parseInt(args[1])];
            initiate(rect);
            rect=makeRec(0,0, rect.length-1, rect[0].length-1, rect);
            rect= makeRec(2, 2, rect.length-3, rect[0].length-3,rect);
            printrect(rect);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    static char[][] makeRec(int beginx, int beginy, int stopx, int stopy, char[][] matrix){
        for(int i =beginx; i<stopx; i++){
            matrix[i][beginy]='#';
            matrix[i][stopy]='#';
        }for(int i=beginy;i<stopy+1;i++){
            matrix[beginx][i]='#';
            matrix[stopx][i]='#';
        }
        return matrix;
    }

    static void printrect(char[][] matrix){
        for(int j =0; j<matrix[0].length;j++){
            for(int i=0;i<matrix.length;i++){
                System.out.print(matrix[i][j]);
            }System.out.println();
        }
    }
    static char[][] initiate(char[][] neu){
        for(int i=0;i<neu.length;i++){
            for(int j=0;j<neu[0].length;j++){
                neu[i][j]=' ';
            }
        }
        return neu;
    }
}