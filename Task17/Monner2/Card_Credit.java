package Monner2;
import java.util.Random;
public class Card_Credit extends Card{
    public Card_Credit(String cardHolder){
        super("0","0","0", cardHolder,Cardtype.CRAIGITCARD,"0");
        StringBuilder cardnumber= new StringBuilder();
        StringBuilder pin= new StringBuilder();
        StringBuilder CVC= new StringBuilder();
        for(int i =0; i< 16;i++){
            cardnumber.append(new Random().nextInt(9));
            if(i>11){
                pin.append(new Random().nextInt(9));
            }if (i>12){
                CVC.append(new Random().nextInt(9));

            }
        }
        this.setCardNumber(cardnumber.toString());
        this.setPin(pin.toString());
        this.setcVC(CVC.toString());
        this.setExpiryDate(java.time.LocalDate.now().toString());
    }

    public Card_Credit(String cardNumber, String pin, String expiryDate, String cardHolder, Cardtype cardtype, String CVC){
        super(cardNumber, pin, expiryDate, cardHolder, cardtype,CVC);
    }

    @Override
    public boolean abortTransaction() {
        System.out.println("unable to abort transaction");
        return false;
    }
    @Override
    public double transaction(double cardnumber, String pin) {
        if (pinIsCorrect(pin)){
            this.addFunds(this.getFunds() - cardnumber);
             return this.getFunds();
    }else{
            System.out.println("wrong pin");
            return 0;
        }
    }


    private boolean pinIsCorrect(String pin){
        if (this.pin.equals(pin)){
            return true;
        }return false;
    }
    /*
    * takes money from this and puts in to_from
    *
    *
    */
    public boolean transaction_card(Card to_from, double amount, String pin){
        if(pinIsCorrect(pin)){
            if (to_from instanceof Card_Credit){
                if(!(this.hasCredit() || ((Card_Credit) to_from).hasCredit())) {
                    this.addFunds(this.getFunds()-amount);
                    to_from.addFunds(to_from.getFunds()+amount);
                }else{
                    System.out.println("insufficient funds: "  + to_from.getFunds() + " or "+ this.getFunds()+ " is less than " + amount);
                    return false;
                }
            }else{
                if(!(this.hasCredit() || ((Card_Savings) to_from).sufficientFunds(amount))){

                    this.addFunds(this.getFunds()-amount);
                    to_from.addFunds(to_from.getFunds()+amount);
                }else{
                    System.out.println("insufficient funds: "  + to_from.getFunds() + " or "+ this.getFunds()+ " is less than " + amount);
                    return false;
                }
            }
            return true;}
        else {
            System.out.println("pin is wrong");
            return false;
        }
    }


    @Override
    public Card get_card() {
        return super.get_card();
    }

    @Override
    public String getUser() {
        return null;
    }

    public boolean hasCredit() {
        return !(this.getFunds() <= 0);
    }
    public void increase_credit(){
        this.addFunds(this.getFunds()+5000);
    }
}