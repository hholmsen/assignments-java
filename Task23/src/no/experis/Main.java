package no.experis;

import java.sql.*;

public class Main {

    public static void main(String[] args) {
        String sql = "SELECT * FROM Employee";
        try (Connection conn = connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            System.out.println("Employees");
            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getInt("id") + "\t" + rs.getString("FirstName") + " " + rs.getString("LastName") + "\t" + rs.getString("Title"));
            }
        } catch (SQLException e) {
            System.out.println("heisi");
            System.out.println(e.getMessage());
        }
    }

    //shamelessly stolen from Craig, who shamelessy stole it from http://www.sqlitetutorial.net/sqlite-java/sqlite-jdbcdriver/
    public static Connection connect() {
        Connection conn = null;
        try {
            String url = "jdbc:sqlite:resources/Northwind_small.sqlite";
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");
            return conn;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
}
