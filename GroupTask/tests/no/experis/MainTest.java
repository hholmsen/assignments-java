package no.experis;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.Assertions.*;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    Main m = new Main();
     Main n = new Main();
    @BeforeEach
    public void setupBoiList() throws Exception{
        m.readBoi("src/no/experis/verify.java");
        n.readBoi("src/no/experis/nonverify.java");
    }
    @Test
    public void testValidJava() throws Exception{
        assertTrue(m.isJavaFile("file.java"));
    }
    @Test
    public void testInvalidJava() throws Exception{
        assertFalse(m.isJavaFile("file.file"));
    }
    @Test
    public void testnumberCurly() throws Exception{
        assertEquals(6, m.numberOfCurlyBois());
    }
    @Test
    public void testnumberSquare() throws Exception{
        assertEquals(12, m.numberOfSquareBois());
    }
    @Test
    public void testnumberCurved() throws Exception{
        assertEquals(4, m.numberOfCurvedBois());
    }
    @Test
    public void testnumberCheeky() throws Exception{
        assertEquals(4, m.numberOfCheekyBois());
    }
    @Test
    public void testRead() throws Exception{
        assertTrue(m.allBois.get(0).equals('{'));
    }
    @Test
    public void testAllEven() throws Exception{
        assertTrue(m.allEven());
    }
    @Test
    public void testAllFalse() throws Exception{
        assertFalse(n.allEven());
    }
    @Test
    public void testCorrectOpenClose(){
        assertTrue(m.correctOpenClose());
        assertFalse(n.correctOpenClose());
    }




}