package com.company;

import java.io.*;
import java.net.*;
import java.util.Scanner;

class Main {
    public static String getHTML(String urlToRead) throws IOException {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        return result.toString();
    }

    public static void main(String[] args) throws IOException
    {
        Scanner sc =  new Scanner(System.in);
        String in;
        do{
            System.out.println("please enter an amiibo character (q to quit)");
            in = sc.nextLine();
            String url = "https://www.amiiboapi.com/api/amiibo/?character=" + in;
            try {
                String[] s = getHTML(url.replace(" ", "%20")).split("amiiboSeries");
                if (s.length == 0) {
                    System.out.println("the amiibo does not exists");
                } else {
                    System.out.println("there exists " + (s.length - 1) + " "+ in+" amiibo");
                    for(int i =1;i<s.length;i++){
                        String url2="https://process.filestackapi.com/AB3oGQtuTSGUTRmKufTk6z/ascii=background:white/" + s[i].split(",")[4].replace("\"image\":", "").replace("\"","");

                        System.out.printf("%n%n%n%s %s %n%n%n", in, i);

                        printascii(getHTML(url2));

                    }
                }
            } catch (FileNotFoundException e) {
                System.out.println("the amiibo does not exists");
            }catch(IOException d){
                System.out.println("the amiibo does not exists");
            }
        } while((!in.equalsIgnoreCase("q")));
    }
    public static void printascii(String s){
        String[] st = s.split("0000\">");
        String[] str = st[1].split("<br>");
        for(int i = 0; i<str.length-1;i++){
            System.out.println(str[i]);
        }
    }
}