import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class PetRockTest {
    private PetRock rocky;

    @BeforeEach
    public void myTestSetup() throws Exception{
        rocky=new PetRock("rocky");
    }

    @Test
    void getName() {
        assertEquals("rocky", rocky.getName());
    }
    @Test
    public void testUnhappyToStart() throws Exception{
        assertFalse(rocky.isHappy());
    }
    @Test
    public void happyAfterPlay() throws Exception{
        rocky.playWithRock();
        assertTrue(rocky.isHappy());
    }
    @Disabled("Exception throwing not yet defined")
    @Test
    public void  nameFail() throws Exception{
        Exception exception = assertThrows(IllegalStateException.class, ()->{
            rocky.getHappyMessage();
        });

        String excpected = "java.lang.IllegalStateException";
        assertTrue(exception.toString().contains(excpected));
    }
    @Test
    public void  name() throws Exception{
        rocky.playWithRock();
        String message = rocky.getHappyMessage();

        String excpected = "im happy.";
        assertTrue(message.equals(excpected));
    }
    @Test
    public void testFaveNumber() throws Exception{
        assertEquals(43, rocky.getFaveNumber());
    }
    @Test
    public void emptyNameFail() throws Exception{
        Exception e = assertThrows(IllegalArgumentException.class, ()->{
            PetRock woofy = new PetRock("");
        });
        String expected = "java.lang.IllegalArgumentException";
        assertEquals(expected, e.toString());

    }
    @Test
    public void waitForHappyTimeout() throws Exception{
        assertTimeoutPreemptively(Duration.ofMillis(100),()->{
            rocky.waitTillHappy();
        });

    }



}