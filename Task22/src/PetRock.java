public class PetRock {
    private String name;
    private boolean happy=false;

    PetRock(String name){
        if (name.isEmpty()){
            throw new IllegalArgumentException();
        }
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public boolean isHappy() {
        return happy;
    }

    public void playWithRock() {
        happy=true;
    }
    public String getHappyMessage(){
       // if(!happy){
       //     throw new IllegalStateException();
       // }
        return  "im happy.";
    }
    public int getFaveNumber(){
        return 43;
    }
    public void waitTillHappy(){
        while(!happy){
            //do nothing;
        }
    }
}
