import Monner2.*;
import java.util.Scanner;
class Main{
    public static void main(String[] a){
        if(a.length<1){
            print_help();
        }else {
            if (a[0].equals("run_tests")){
                System.out.printf("This is a testprogram testing all the features of the payment-system, %nit creates cards and cash, and allows transactions between all types of payment methods(i.e. from credit card to cash).%n a pin is genererated for each card and is used when transacting between cards %n");

                test_credit();
                test_cash();
                test_transactions();
                test_wrong_pin();
                test_over_withdraw_credit();
                test_over_withdraw_savings();
                test_transaction_cash_credit();
                test_transaction_credit_cash();
                test_transaction_savings_credit();
            }else if(a[0].equals("transaction")){
                run_test_transaction();
            }else{
                print_help();
            }
        }



    }
    static void run_test_transaction(){
        Scanner sc=new Scanner(System.in);
        System.out.println("Hello do you want a new card?(y/n)");
        if(sc.nextLine().equals("y")){
            System.out.println("Nice! do you want a savings or credit card?(savings/credit)");
            String hei = sc.nextLine();
            if(hei.equals("savings")){
                System.out.println("Great! now enter your name");
                String name= sc.nextLine();
                Card_Savings card=new Card_Savings(name);
                card.addFunds(5000);
                System.out.printf("Card created and 5000 currencies added:%n Card number:%s%n Card pin:%s%n", card.getCardNumber(), card.getPin());
                System.out.println("do you want to buy a coke for 3000 currencies?(y/n)");
                if(sc.nextLine().equalsIgnoreCase("y")){
                    System.out.println("please enter your pin:");
                    String pin = sc.nextLine();
                    card.transaction(3000, pin);
                    System.out.println("your current balance is " + card.getFunds());
                }else{
                    System.out.println("bye");
                }

            }if(hei.equals("credit")){
                System.out.println("Great! now enter your name");
                String name= sc.nextLine();
                Card_Credit card=new Card_Credit(name);
                card.increase_credit();
                System.out.printf("Card created and 5000 currencies credited:%n Card number:%s%n Card pin:%s%n", card.getCardNumber(), card.getPin());
                System.out.println("do you want to buy a coke for 3000 currencies?(y/n)");
                if(sc.nextLine().equalsIgnoreCase("y")){
                    System.out.println("please enter your pin:");
                    card.transaction(3000, sc.next());
                    System.out.println("your current balance is " + card.getFunds());
                }else{
                    System.out.println("bye");
                }
            }
        }else{
            System.out.println("okey, cash then?");
            if (sc.nextLine().equals("y")){
                System.out.println("here is 5000 currencies, buy a coke?");
                Cash money=new Cash();
                money.addAmount(5000);
                if (sc.nextLine().equals("y")){
                    money.change(3000);
                    System.out.println("current balance is" + money.getFunds());
                }else{
                    System.out.println("bye");
                }
            }
        }

    }
    static void print_help(){
        System.out.printf("USAGE:%n arg1: either (run_tests) or (transaction)%n ");
    }
    static void test_cash(){
        System.out.printf("######### Test Cash ###########%n");
        Cash wallet = new Cash();
        wallet.add05();
        wallet.add50();
        wallet.add500();
        System.out.println(wallet.getFunds());
        wallet.change(250);
        System.out.println(wallet.getFunds());
        System.out.printf("###############################%n%n");

    }
    static void test_credit(){
        System.out.printf("######### Test Credit ###########%n");
        Card_Credit h = new Card_Credit("Henrik Holmsen");
        Card_Credit c = new Card_Credit("Craig Marais");
        System.out.printf("Created new card:%nCardholder %s %nPincode:%s%nCVC: %s%nCardType:%s%nExpires on: %s%n",h.getCardHolder(), h.getPin(), h.getcVC(), h.getCardtype(), h.getExpiryDate());
        System.out.printf("###############################%n%n");

    }

    static void test_transactions(){
        System.out.printf("############### testing basic transactions and card genereation ###############%n");
        Card_Credit h = new Card_Credit("Henrik Holmsen");
        Card_Credit c = new Card_Credit("Craig Marais");
        System.out.println(h.getcVC() + " CVC");
        System.out.println(h.getCardNumber() + " Cardnumber");

        h.increase_credit();
        c.increase_credit();
        h.addFunds(5000);
        c.transaction_card(h,500, c.getPin());
        h.transaction_card(c, 750, h.getPin());

        Card_Savings m = new Card_Savings("Magnus Nordin");
        m.addFunds(25000);
        m.transaction_card(h, 10000, m.getPin());
        h.transaction_card(m, 10000, h.getPin());
        h.transaction_card(c, 10000, h.getPin());
        h.transaction_card(c, 10000, c.getPin());
        h.transaction_card(c, 10000, h.getPin());
        System.out.printf("%s, with cardnumber %s, with funds %f %n",m.getCardHolder(),m.getCardNumber(), m.getFunds());
        System.out.printf("%s, with cardnumber %s, with funds %f %n",h.getCardHolder(),h.getCardNumber(), h.getFunds());
        System.out.printf("%s, with cardnumber %s, with funds %f %n",c.getCardHolder(),c.getCardNumber(), c.getFunds());

        if(c.getFunds()==5000 && h.getFunds()==15000){
            System.out.println("############# test_transaction_passed ##################3");
        }else{
            System.out.println("################# test_transaction_failed ###################");
        }
        System.out.printf("####################################%n%n");
    }
    static void test_wrong_pin(){
        System.out.printf("######### Test Wrong Pin ###########%n");
        Card_Credit h = new Card_Credit("Henrik Holmsen");
        Card_Credit c = new Card_Credit("Craig Marais");
        h.increase_credit();
        c.increase_credit();
        System.out.println(h.getFunds());
        boolean correct = h.transaction_card(c,100, c.getPin());
        if(!correct){
            System.out.printf("################ Test Success ###############%n");
        }else{
            System.out.printf("################# Test Failed ##############%n");
        }

        System.out.printf("###############################%n%n");

    }
    static void test_over_withdraw_credit(){
        System.out.printf("######### Test Over withdraw Credit ###########%n");
        Card_Credit h = new Card_Credit("Henrik Holmsen");
        Card_Credit c = new Card_Credit("Craig Marais");
        h.increase_credit();
        h.transaction_card(c, 1000,h.getPin());
        boolean correct = h.transaction_card(c, 1000,h.getPin());
        if(!correct){
            System.out.printf("################ Test Success ###############%n");
        }else{
            System.out.printf("################# Test Failed ##############%n");
        }
        System.out.printf("###############################%n%n");

    }static void test_over_withdraw_savings(){
        System.out.printf("######### Test Over withdraw Savings ###########%n");
        System.out.printf("###############################%n");

    }static void test_transaction_cash_credit(){
        System.out.printf("######### Test transaction Cash Credit ###########%n");
        // cash to card, and card to cash transactions need to be handleled from the main class.
        Cash money= new Cash();
        Card_Credit card = new Card_Credit("Bror Hernes");
        money.addAmount(500);
        double transacted = 250;
        double money_left = money.transaction(transacted,"0");
        card.addFunds(money_left);
        if(card.getFunds()==money.getFunds()){
            System.out.printf("################ Test Success ###############%n");
        }else{
            System.out.printf("################# Test Failed ##############%n");
        }
        System.out.printf("###############################%n%n");

    }static void test_transaction_credit_cash(){
        System.out.printf("######### Test transaction Credit cash ###########%n");
        Card_Credit card = new Card_Credit("Bror Hernes");
        Cash money=new Cash();
        card.increase_credit();
        double transaction=2500;
        double left = card.transaction(transaction, card.getPin());
        money.addAmount(transaction);
        if(money.getFunds()==left){
            System.out.printf("################ Test Success ###############%n");
        }else{
            System.out.printf("################# Test Failed ##############%n");
        }
        System.out.printf("###############################%n%n");

    }static void test_transaction_savings_credit(){
        System.out.printf("######### Test Transaction Savings Credit ###########%n");
        Card_Savings card = new Card_Savings("Bror Hernes");
        Cash money=new Cash();
        card.addFunds(5000);
        double transaction=2500;
        double left = card.transaction(transaction, card.getPin());
        money.addAmount(transaction);
        if(money.getFunds()==left){
            System.out.printf("################ Test Success ###############%n");
        }else{
            System.out.printf("################# Test Failed ##############%n");
        }
        System.out.printf("###############################%n%n");

    }
}