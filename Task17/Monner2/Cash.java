package Monner2;
import java.util.HashMap;

public class Cash implements Imoney{
    HashMap<Double, Integer> funds = new HashMap<Double, Integer>();

    public Cash(){
        this.funds.put(0.5,0);
        this.funds.put(1.0,0);
        this.funds.put(5.0,0);
        this.funds.put(10.0,0);
        this.funds.put(20.0,0);
        this.funds.put(50.0,0);
        this.funds.put(100.0,0);
        this.funds.put(200.0,0);
        this.funds.put(500.0,0);
        this.funds.put(1000.0,0);
    }

    @Override
    public double transaction(double amount, String pin) {
        change(amount);
        return this.getFunds();
    }

    @Override
    public double getFunds() {

        double total=0;
        for(double key : this.funds.keySet()){
            total+=key*funds.get(key);
            //System.out.println(key +" " + funds.get(key));
        }
        return total;
    }

    public double addAmount(double amount){
        while(amount != 0) {
            if (amount % 1000 == 0) {
                amount = amount - 1000;
                this.funds.put(1000.0, this.funds.get(1000.0) + 1);
            } else if (amount % 500 == 0) {
                amount = amount - 500;
                this.funds.put(500.0, this.funds.get(500.0) + 1);
            } else if (amount % 200 == 0) {
                amount = amount - 200;
                this.funds.put(200.0, this.funds.get(200.0) + 1);
            } else if (amount % 100 == 0) {
                amount = amount - 100;
                this.funds.put(100.0, this.funds.get(100.0) + 1);
            } else if (amount % 50 == 0) {
                amount = amount - 50;
                this.funds.put(50.0, this.funds.get(50.0) + 1);
            } else if (amount % 20 == 0) {
                amount = amount - 20;
                this.funds.put(20.0, this.funds.get(20.0) + 1);
            } else if (amount % 10 == 0) {
                amount = amount - 10;
                this.funds.put(10.0, this.funds.get(10.0) + 1);
            } else if (amount % 5 == 0) {
                amount = amount - 5;
                this.funds.put(5.0, this.funds.get(5.0) + 1);
            } else if (amount % 1 == 0) {
                amount = amount - 1;
                this.funds.put(1.0, this.funds.get(1.0) + 1);
            } else if (amount % 0.5 == 0) {
                amount = amount - 0.5;
                this.funds.put(0.5, this.funds.get(0.5) + 1);
            } else {
                System.out.println("unable to change");
                break;
            }
        }
        return amount;
    }

    public void add05(){
        this.funds.put(0.5, this.funds.get(0.5) +1);
    }
    public void add1(){
        this.funds.put(1.0, this.funds.get(1.0) +1);
    }
    public void add5(){
        this.funds.put(5.0, this.funds.get(5.0) +1);
    }
    public void add10(){
        this.funds.put(10.0, this.funds.get(10.0) +1);
    }
    public void add20(){
        this.funds.put(20.0, this.funds.get(20.0) +1);
    }
    public void add50(){
        this.funds.put(50.0, this.funds.get(50.0) +1);
    }
    public void add100(){
        this.funds.put(100.0, this.funds.get(100.0) +1);
    }
    public void add200(){
        this.funds.put(200.0, this.funds.get(200.0) +1);
    }
    public void add500(){
        this.funds.put(500.0, this.funds.get(500.0) +1);
    }
    public void add1000(){
        this.funds.put(1000.0, this.funds.get(1000.0) +1);
    }

    synchronized public void change(double amount){
        if(amount>this.getFunds()){
            System.out.println("insufficient funds");

        }else {
            System.out.println(this.funds.size());
            //its the uglies
            while(amount != 0){
                if (amount % 1000 ==0){
                    amount = amount -1000;
                    this.funds.put(1000.0, this.funds.get(1000.0) -1);
                }else if(amount % 500 == 0){
                    amount = amount -500;
                    this.funds.put(500.0, this.funds.get(500.0) -1);
                }else if(amount % 200 == 0){
                    amount = amount -200;
                    this.funds.put(200.0, this.funds.get(200.0) -1);
                }else if(amount % 100 == 0){
                    amount = amount -100;
                    this.funds.put(100.0, this.funds.get(100.0) -1);
                }else if(amount % 50 == 0){
                    amount = amount -50;
                    this.funds.put(50.0, this.funds.get(50.0) -1);
                }else if(amount % 20 == 0){
                    amount = amount -20;
                    this.funds.put(20.0, this.funds.get(20.0) -1);
                }else if(amount % 10 == 0){
                    amount = amount -10;
                    this.funds.put(10.0, this.funds.get(10.0) -1);
                }else if(amount % 5 == 0){
                    amount = amount -5;
                    this.funds.put(5.0, this.funds.get(5.0) -1);
                }else if(amount % 1 == 0){
                    amount = amount -1;
                    this.funds.put(1.0, this.funds.get(1.0) -1);
                }else if(amount % 0.5 == 0){
                    amount = amount -0.5;
                    this.funds.put(0.5, this.funds.get(0.5) -1);
                }else{
                    System.out.println("unable to change");
                    break;
                }
            }
        }
    }
    @Override
    public String getUser() {
        return null;
    }

    @Override
    public boolean abortTransaction() {
        return false;
    }

    @Override
    public void addFunds(double funds) {

    }
}
