import java.io.*;
import java.util.ArrayList;

class enhanced_wc{
    public static void main(String[] a){
        if(a.length>2){
            if(a[0].equalsIgnoreCase("open")){
            try{
                File file = new File(a[1]);
                BufferedReader br = new BufferedReader(new FileReader(file));
                System.out.println("Filename: " + file.toString()); 
                System.out.println("Filesize: " + Integer.toBinaryString((int)(file.length()*8)) + " ones and zeroes (in binary), or " + file.length() + " bytes, Craig!"); 
                int line=0;
                String st;
                ArrayList<String[]> sentences= new ArrayList<>();
                while ((st = br.readLine()) != null) {
                  sentences.add(st.split(" "));
                  line++;
                }
                System.out.println("Number of lines: " + line);
                br.close(); 
                int search_hits=0;
                for(int i =0;i<sentences.size();i++){
                    for(int j=0; j< sentences.get(i).length;j++){
                        if(sentences.get(i)[j].toLowerCase().contains(a[2].toLowerCase())){
                            search_hits++;
                        }
                        
                    }
                }
                System.out.println("The word " + a[2] + " appears " + search_hits + " times:)");

            }catch(FileNotFoundException e){
                 System.out.println("not a valid filename.");
            }catch(IOException e){
                System.out.println("its not a bug, its a feature ;)");
            }
        }else {
            print_help();
        }

        }
        else{
            print_help();
        }
    }
    static void print_help(){
        System.out.printf("Usage: %n arg1: Open (opens a file) %n arg2: Filename (i.e.small.txt) %n arg3: Search parameter%n");
    }
}