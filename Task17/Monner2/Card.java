package Monner2;

abstract class Card implements Imoney{
    private String cardNumber;
    protected String pin;
    private double funds;
    private String expiryDate;
    private String cardHolder;
    private Cardtype cardtype;
    private String cVC;

    public Card(String cardNumber, String pin, String expiryDate, String cardHolder, Cardtype cardtype, String CVC){
        this.cardNumber=cardNumber;
        this.cardHolder=cardHolder;
        this.pin=pin;
        this.expiryDate=expiryDate;
        this.cardtype=cardtype;
        this.cVC=CVC;
        this.funds=0.0;
    }

    public Cardtype getCardtype() {
        return this.cardtype;
    }

    @Override
    public double getFunds() {
        return this.funds;
    }

    public String getCardHolder() {
        return this.cardHolder;
    }

    @Override
    public String getUser() {
        return null;
    }

    public String getExpiryDate() {
        return this.expiryDate;
    }

    public String getcVC() {
        return this.cVC;
    }

    public String getPin() {
        return this.pin;
    }


    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public void setCardtype(Cardtype cardtype) {
        this.cardtype = cardtype;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public void addFunds(double funds) {
        this.funds = funds;
    }

    public void setcVC(String cVC) {
        this.cVC = cVC;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Card get_card(){
        return this;
    }
    public String getCardNumber(){
        return this.cardNumber;
    }



    
}