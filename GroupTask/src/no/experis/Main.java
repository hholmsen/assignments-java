package no.experis;

import java.io.*;
import java.util.ArrayList;

public class Main {
     ArrayList<Character> allBois= new ArrayList<>();

    public void main(String[] args) {

        // write your code here


    }
    public  boolean allEven(){
        return numberOfCheekyBois() % 2 == 0 && numberOfCurlyBois() % 2 == 0 && numberOfCurvedBois() % 2 == 0 && numberOfSquareBois() % 2 == 0;
    }

    public  boolean isJavaFile(String filename){
        return filename.endsWith(".java");
    }
    public  int numberOfCurlyBois( ){
        int num=0;
        for(char c : allBois){

            if(c=='{' || c=='}'){
                num++;
            }
        }
        return num;
    }
    public  int numberOfSquareBois( ){
        int num=0;
        for(char c : allBois){
            if(c=='[' || c==']'){
                num++;
            }
        }
        return num;
    }
    public  int numberOfCurvedBois( ){
        int num=0;
        for(char c : allBois){
            if(c=='(' || c==')'){
                num++;
            }
        }
        return num;
    }
    public  int numberOfCheekyBois( ){
        int num=0;
        for(char c : allBois){
            if(c=='<' || c=='>'){
                num++;
            }
        }
        return num;
    }
    public  void readBoi(String validFileName){
        String stringBois;
        try {
            FileReader f = new FileReader(validFileName);
            BufferedReader b = new BufferedReader(f);

            while((stringBois = b.readLine())!=null){
                for(int i = 0;i<stringBois.length();i++){
                    char c = stringBois.charAt(i);
                    if(c=='{' || c=='}' || c=='('||c==')'|| c=='[' ||c==']' ||c=='<'||c=='>'){
                        allBois.add(c);
                    }
                }
            }
            f.close();
            b.close();

        }catch(IOException e) {
            e.printStackTrace();
        }
    }
    public boolean correctOpenClose(){
        boolean ret = true;
        int i=0;
        for(char c: allBois){
            int numOpen=0;
            int num_closed=0;
            if(c=='{'||c=='('||c=='['){
                for(int j= i;j<allBois.size()-i;j++){
                    if(allBois.get(j) == pair(c)){
                        System.out.println(numOpen + " " + num_closed);
                        if(numOpen!=num_closed){
                            ret =  false;
                        }
                        break;
                    }else if(allBois.get(j)=='{'||allBois.get(j)=='('||allBois.get(j)=='['){
                        numOpen++;
                    }else if((allBois.get(j)==pair('{')||allBois.get(j)==pair('(')||allBois.get(j)==pair('['))){
                        num_closed++;
                    }

                }
            }
            i++;
        }
        return ret;
    }
    public char pair(char c){
        if(c=='{'){
            return '}';
        }else if(c=='('){
            return ')';
        }else if(c=='['){
            return ']';
        }return ' ';
    }


}
