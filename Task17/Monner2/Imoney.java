package Monner2;
//interface used as an academic exercise
interface Imoney{

    double transaction(double amount, String pin);

    double getFunds();

    String getUser();

    boolean abortTransaction();

    void addFunds(double funds);

}