package Monner2;

import java.util.Random;

public class Card_Savings extends Card{

    public Card_Savings(String cardHolder){
        super("0","0","0", cardHolder,Cardtype.VISA,"0");
        StringBuilder cardnumber= new StringBuilder();
        StringBuilder pin= new StringBuilder();
        StringBuilder CVC= new StringBuilder();
        for(int i =0; i< 16;i++){
            cardnumber.append(new Random().nextInt(9));
            if(i>11){
                pin.append(new Random().nextInt(9));
            }if (i>12){
                CVC.append(new Random().nextInt(9));

            }
        }
        this.setCardNumber(cardnumber.toString());
        this.setPin(pin.toString());
        this.setcVC(CVC.toString());
        this.setExpiryDate(java.time.LocalDate.now().toString());
    }

    public Card_Savings(String cardNumber, String pin, String expiryDate, String cardHolder, Cardtype cardtype, String CVC){
        super(cardNumber, pin, expiryDate, cardHolder, cardtype,CVC);
    }

    @Override
    public boolean abortTransaction() {
        System.out.println("unable to abort transaction");
        return false;
    }

    @Override
    public double transaction(double cardnumber, String pin) {
        if (pinIsCorrect(pin)){
            this.addFunds(this.getFunds() - cardnumber);
            return this.getFunds();
        }else{
            System.out.println("wrong pin");
            return 0;
        }
    }
    private boolean pinIsCorrect(String pin){
        if (this.pin.equals(pin)){
            return true;
        }return false;
    }

    public boolean transaction_card(Card c, double amount, String pin){
        if(pinIsCorrect(pin)){
        if (c instanceof Card_Credit){
            if((this.sufficientFunds(amount) || ((Card_Credit) c).hasCredit())) {
                this.addFunds(this.getFunds()-amount);
                c.addFunds(c.getFunds()+amount);
            }else{
                System.out.println("insufficient funds: "  + c.getFunds() + " or "+ this.getFunds()+ " is less than " + amount);
                return false;
            }
        }else{
            if(!(this.sufficientFunds(amount) || ((Card_Savings) c).sufficientFunds(amount))){
                this.addFunds(this.getFunds()-amount);
                c.addFunds(c.getFunds()+amount);
            }else{
                System.out.println("insufficient funds: "  + c.getFunds() + " or "+ this.getFunds()+ " is less than " + amount);
                return false;
            }
        }
        return true;}else{
            System.out.println("pin is wrong");
            return false;
        }

    }

    @Override
    public Card get_card() {
        return super.get_card();
    }


    @Override
    public String getUser() {
        return null;
    }
    public boolean sufficientFunds(double toPay){
        return !(this.getFunds() < toPay);
    }


}