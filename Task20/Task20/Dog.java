package Task20;

import java.util.ArrayList;
import java.util.List;

public class Dog extends Omnivore {
    static ArrayList<Moveable> moves = new ArrayList<Moveable>(List.of(new Walk(), new Swim(),  new Run(), new Climb()));

    Dog() {
        super(moves, "Dog");
    }
}