How to run and compile the different files.
===========================================

* Task 11:
----------
	* Run using: java hip_to_be_square
	* Compile using: javac hip_to_be_squar.java
* Task 12:
----------
	* Run using: java hip_to_be_rects
	* Compile using: javac hip_to_be_rects.java
* Task 13:
----------
	* Run with java
	* Compile with javac
* Task 14:
----------
	* Run with java 
	* Compile with javac
* Task 15:
----------
	* Run and compile from external folder as Person and search is a package
* Task 16:
----------
	* Run using java and compile using javac
