package Task15;
class Person{
    private String firstName;
    private String lastName;
    private String phone;
    private String eMail;

    public Person(){
        this.firstName="Craig";
        this.lastName="Marais";
        this.phone="+4790366866";
        this.eMail="craig.marais@noroff.no";
    }
    public Person(String firstName, String lastName, String phone, String eMail){
        this.firstName=firstName;
        this.lastName=lastName;
        this.phone=phone;
        this.eMail=eMail;
    }

    public String getFirstName(){
        return this.firstName;
    }
    
    public String getLastName(){
        return this.lastName;
    }
    
    public String getPhone(){
        return this.phone;
    }
    public String  getEmail(){
        return this.eMail;
    }
}