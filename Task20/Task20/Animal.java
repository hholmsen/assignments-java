package Task20;

import java.util.ArrayList;
import java.util.Random;

public abstract class Animal{
    ArrayList<Moveable> moves = new ArrayList<>();
    String name;
    
    Animal(ArrayList<Moveable> moves, String name){
        this.moves=moves;
        this.name =name;
    }

    void printAll(){
        System.out.printf("The %s can ", this.name);
        for(int i =0;i<moves.size();i++){
            System.out.printf("%s, ", moves.get(i).getMove());
        }
        System.out.println("");
    }
    void printRandom(){
        Random r = new Random();
        int i = r.nextInt(moves.size());
        moves.get(i).move(this.name);
    }

}