public class BMI_calcu{
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String YELLOW = "\033[0;33m";  // YELLOW
    public static void main(String[] a){
        try{
        if(a.length>1){
            double height=Double.parseDouble(a[1]);

            if (Double.parseDouble(a[1])>100){
                printf("Centimeters selected\n");
                height=height/100;
            }
            //weight first, height second
            double bmi=Double.parseDouble(a[0])/(height*height);
            System.out.printf( "BMI: %.2f %n", bmi);
            if(bmi<18.5){
                printf(new String(ANSI_RED + "You are an underweight boi\n" + ANSI_RESET));
            }else if(bmi<=24.5){
                printf(new String( ANSI_GREEN + "You are a normal boi \n" + ANSI_RESET));
            }else if(bmi<=29.9){
                printf(new String(YELLOW + "You are an overweight boi \n" +ANSI_RESET));
            }else{
                printf(new String(ANSI_RED + "You are an obese boi \n" + ANSI_RESET));
            }
        }else {
            printf("Please enter two arguments (first weigth(in kilograms) and then height (in meters))\n");
        }
    }catch(NumberFormatException e){
        printf("please enter a valid number\n ");
    }catch(Exception e){
        printf("Something went wrong\n");
    }
    }


    static void printf(String s){
        System.out.printf(s);
    }
}