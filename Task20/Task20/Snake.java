package Task20;

import java.util.ArrayList;
import java.util.List;

public class Snake extends Carnivore {
    static ArrayList<Moveable> moves = new ArrayList<Moveable>(List.of(new Swim(), new Slither(), new Climb()));

    Snake() {
        super(moves, "Snake");
    }
}