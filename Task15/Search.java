package Task15;
import java.util.ArrayList;
class Search{

    public static void main(String [] a){
        Person person= new Person();
        System.out.print(person.getFirstName());
        ArrayList<Person> people= new ArrayList<>();
        people.add(new Person("Henrik", "Holmsen", "45472039", "hholmsen@icloud.com"));
        people.add(new Person("Bror", "Hernes", "+6942042069", "bror.hernes@no.experis.com"));
        people.add(new Person("Magnus", "Nordin", "69420420", "magnus.nordin@no.experis.com"));
        people.add(new Person("Helene", "Boge","42069420", "helene.boge@no.experis.com"));

        int flip=0;

        if(a.length==0){
            System.out.printf("input an argument%n");
        }else{
            for(int i =0; i<people.size(); i++){
                if(people.get(i).getFirstName().toLowerCase().contains(a[0].toLowerCase()) 
                || people.get(i).getLastName().toLowerCase().contains(a[0].toLowerCase())){
                    flip=1;
                    printf(new String(people.get(i).getFirstName()+ people.get(i).getLastName() + " is contained in the list!\n"));
                    
                }
        }
        if(flip==0){
            printf("Not contained in the list");
        }
    }
    }
    static void printf(String s){
        System.out.printf(s);
    }
}